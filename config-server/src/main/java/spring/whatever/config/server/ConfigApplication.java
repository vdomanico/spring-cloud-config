package spring.whatever.config.server;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class ConfigApplication implements CommandLineRunner {

    @Value("${spring.cloud.config.server.git.uri}") String configDirectory;

    public static void main(String[] args) {
        SpringApplication.run(ConfigApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        LoggerFactory
                .getLogger("ROOT")
                .info(String.format("Config properties directory is : %s", configDirectory));
    }
}
