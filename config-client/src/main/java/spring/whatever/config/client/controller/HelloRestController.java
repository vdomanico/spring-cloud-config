package spring.whatever.config.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HelloRestController {

    private final Environment environment;

    @Autowired
    public HelloRestController(Environment environment) {
        this.environment = environment;
    }


    @GetMapping("/hello")
    public Map<String, String> hello(){
        Map<String, String> map = new HashMap<>();
        map.put("hello", environment.getProperty("hello"));
        map.put("custom-property", environment.getProperty("custom-property"));
        return map;

    }

}
