package spring.whatever.config.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;

@SpringBootApplication
@RefreshScope
public class ClientApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger("ROOT");

    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
    }

    private final Environment environment;

    @Autowired
    public ClientApplication(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void run(String... args){
        log.info("Hello Property Value is: {}", environment.getProperty("hello"));
    }
}

